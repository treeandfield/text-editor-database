(function ($) {
  // custom css expression for a case-insensitive contains()

  jQuery.expr[':'].Contains = function(a,i,m){
      return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
  };
  
  
  $.fn.extend({ 
	    
	     listFilter: function(target) { 
			
		    $(this).change( function () {

		        var list = $(target);
		        var filter = $(this).val();
		        
		        if(filter) {
		          $(list).find(".srch:not(:Contains(" + filter + "))").parent().hide();
		          $(list).find(".srch:Contains(" + filter + ")").parent().show();
		        } else {
		          $(list).find("li").show();
		        }
		        return false;
				}).keyup( function () {
		        	$(this).change();
				});
			}
  });
  
  
}(jQuery));